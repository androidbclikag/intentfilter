package com.example.intentfilter

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    val requestCode = 20
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        init()
    }

    private fun init() {
        addressButton.setOnClickListener {
            fillAddress()

        }
        signUpButton.setOnClickListener {
            signUp()
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 20 && resultCode == Activity.RESULT_OK) {
            val address = data!!.extras?.getString("address", "")
            addressTextView.text = address

        }
    }


    private fun fillAddress() {
        val intent = Intent(this, ChooseAddressActivity::class.java)
        startActivityForResult(intent, requestCode)
    }

    private fun signUp() {
        val userProfile = UserModel(
            userNameEditText.text.toString(),
            firstNameEditText.text.toString(),
            lastNameEditText.text.toString(),
            ageEditText.text.toString().toInt(),
            addressTextView.text.toString()

        )
        val intent = Intent(this, DashBoardActivity::class.java)
        intent.putExtra("userProfile", userProfile)
        startActivity(intent)

    }
}
