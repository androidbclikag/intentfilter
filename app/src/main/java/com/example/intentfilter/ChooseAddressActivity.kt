package com.example.intentfilter

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_choose_address.*

class ChooseAddressActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_address)
        init()
    }


    private fun init(){
        sentAddressButton.setOnClickListener {
            val address = addressEditText.text.toString()
            val intent = intent.putExtra("address", address)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

    }
}
