package com.example.intentfilter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_dash_board.*

class DashBoardActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dash_board)
        init()
    }


    private fun init() {

        val userProfile = intent.extras!!.getParcelable<UserModel>("userProfile")
        userTextView.text = userProfile!!.userName.toString()
        firstNameTextView.text = userProfile.firstName.toString()
        lastNameTextView.text = userProfile.lastName.toString()
        ageTextView.text = userProfile.age.toString()
        addressTextView.text = userProfile.address.toString()

}

}
